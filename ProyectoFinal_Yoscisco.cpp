/*
	Name: Proyecto Final - Registros de Red Social Escolar
	Copyright: UJP
	Author: Francisco Everardo Dan Eumby Hernandez Lopez y Yosef Herrera Cazarin
	Date: 10/10/20 16:27
	Description: Estructura que guarda los datos de una red social para alumnos, y se mostrar en pantalla, 
la estructura tiene (cedula, nombre, apellido, edad, carrera, lugar de nacimiento, dirección electronica y teléfono).
*/
#include<iostream>
#include<string>
#include<fstream>
#include<stdlib.h>

using namespace std;
#define MAX 100 /*Constante*/
 
int main(void)
   {
    /*Declara las variables para los ciclo for*/
    int i = 0, n = 0;
 
    /*Declara estructura persona*/
    struct persona
	{
        char matricula[14];
        char nombre[20];
        char apellido[20];
        char apellido2[20];
        int edad;
        char carrera[20];
        char lugar[50];
        char correo[50];
        long long telefono;
        char usuario[50];
        char contrasenia[50];
    };
    /*Declara alumno, arreglo de la estructura persona*/
    struct persona alumno[MAX];
    
    /*Mensajes iniciales*/
 	cout<<"\nYoscisco (c) [version 1010.2020]\nTodos los derechos reservados.\n ";
 	cout<<"\nBienvenido al Registro para Red Social Escolar\n";
    
	/*Se pide cuantos registros de alumnos se guardaran*/
    cout<<"\nEscriba el numero de registros a ingresar: ";
    cin>> n;
    cout<<"\nIngresar datos sin espacios y mayusculas ";
    
 
   
    /*Ciclo for que va a recorrer según la cantidad escrita anteriormente*/
    for (i = 0; i < n; i++)
	{	
		cout<<"\n------------------------------  Registro  "<< i+1<<"  ------------------------------";
		
		cout<<"\nEscriba la Matricula:";
        cin>> alumno[i].matricula;
   
        cout<<"\nEscriba el Nombre:";
        cin>> alumno[i].nombre;
   
        cout<<"\nEscriba el pimer Apellido:";
        cin>> alumno[i].apellido;
        
		cout<<"\nEscriba el segundo Apellido:";
        cin>> alumno[i].apellido2;
        
		cout<<"\nEscriba la Edad:";
        cin>> alumno[i].edad;
   
        cout<<"\nEscriba la Carrera:";
        cin>> alumno[i].carrera;
   
        cout<<"\nEscriba el lugar de nacimiento:";
        cin>> alumno[i].lugar;
   
        cout<<"\nEscriba el correo:";
        cin>> alumno[i].correo;
   
        cout<<"\nEscriba el telefono:";
        cin>> alumno[i].telefono;
        
        cout<<"\nEscriba el identificador de usuario de la cuenta:";
        cin>> alumno[i].usuario;
        
        cout<<"\nEscriba la contraseña de la red social:";
        cin>> alumno[i].contrasenia;
    

    
}
    
    cout<<"Las redes sociales registradas son : "<<endl;
    /*Ciclo for que muestra el listado de registro ingresados*/
    for (i = 0; i < n; i++){
        /*Se llama al arreglo alumno seguido de la variable cedula*/
        cout<<alumno[i].matricula;
        cout<<"\t"<<alumno[i].nombre;
        cout<<"\t"<<alumno[i].apellido;
        cout<<"\t"<<alumno[i].apellido2;
        cout<<"\t"<<alumno[i].edad;
        cout<<"\t"<<alumno[i].carrera;
        cout<<"\t"<<alumno[i].lugar;
        cout<<"\t"<<alumno[i].correo;
        cout<<"\t"<<alumno[i].telefono;
        cout<<"\t"<<alumno[i].usuario;
        cout<<"\t"<<alumno[i].contrasenia<<"\n\n";
        
        
    }   
    ofstream archivo;
   
   archivo.open("RedSocial2.txt");
   for (i = 0; i < n; i++)
   {
        cout<<"\n------------------------------  Registro  "<< i+1<<"  ------------------------------";
   
   archivo << "\nMatricula: " << alumno[i].matricula << endl;
   archivo << "Nombre de alumno: " << alumno[i].nombre << endl;
   archivo << "Primer Apellido del alumno: " << alumno[i].apellido << endl;
   archivo << "Primer Apellido del alumno: " << alumno[i].apellido2 << endl;
   archivo << "Edad: " << alumno[i].edad << endl;
   archivo << "Carrera: " << alumno[i].carrera << endl;
   archivo << "Lugar: " <<alumno[i].lugar << endl;
   archivo << "Correo electronico: " << alumno[i].correo << endl;
   archivo << "Telefono: " << alumno[i].telefono << endl;
   archivo << "Nombre de la red social: " << alumno[i].usuario << endl;
   archivo << "Contraseña de la red social: " <<alumno[i].contrasenia  << endl;
   cout << "El registro de los alumnos se ha realizado satisfactoriamente, debe abrir el archivo RedSocial2.txt en la carpeta del programa para ver los datos almacenados." << endl;
}
   
   
   system("pause>>null");
   return 0;
}